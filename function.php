<?php

/**
 * include file php
 *
 * @since astro framework 1.0.1
 * @return directory  plugin/current_directory
 */
if (!function_exists('astro_get_file')) {
    function astro_get_file($part)
    {
        include dirname(plugin_basename(__FILE__)) . "{$part}.php";
    }

}

/**
 * include file php form base plugin directory
 *
 * @since astro framework 1.0.1
 * @return directory plugin/
 */
if (!function_exists('astro_get_template_part')) {
    function astro_get_template_part($part)
    {
        include plugin_dir_path(__FILE__) ."{$part}.php";
    }

}

/**
 * Get static asset from assets folder
 *
 * @since astro framework 1.0.1
 * @return directory plugin/assets
 */
if (!function_exists('astro_get_plugin_asset')) {

    function astro_get_plugin_asset($url)
    {
        return plugin_dir_url(__FILE__) . "assets/{$url}";
    }
}


/*=================================================
 * LIMIT THE CUNTENT
/*================================================= */
if (!function_exists('astro_the_content')) {
    function astro_the_content($excerpt_length = '')
    {
        if (empty($excerpt_length)) {
            $excerpt_length = 18;
        }

        $the_excerpt = strip_tags(strip_shortcodes(get_the_content()));
        $words = explode(' ', $the_excerpt, $excerpt_length + 1);

        if (count($words) > $excerpt_length):
            array_pop($words);
            $the_excerpt = implode(' ', $words);
            $the_excerpt .= '...';
        endif;

        echo $the_excerpt;
    }
}

/*=================================================
 *  CONDITION TAGS
/*=================================================
 * @since 1.0.0
 * @desc cek plugin WooCommerce active
 * @param page WooCommerce page $page
 * @return boolean
 */

if (!function_exists('astro_is_woocommerce')) {
    function astro_is_woocommerce($page = '')
    {
        if (class_exists('WooCommerce')) {
            // Rertuns true on WooCommerce Plugins active
            if (empty($page)) {
                return true;
            }

            // all template WooCommerce but cart and checkout not include
            if ($page == 'pages' && is_woocommerce()) {
                return true;
            }

            // Returns true when on the product archive page (shop).
            if ($page == 'shop' && is_shop()) {
                return true;
            }

            if ($page == 'product' && is_product()) {
                return true;
            }

            // Returns true on the customer’s account pages.
            if ($page == 'account_page' && is_account_page()) {
                return true;
            }

            // Returns true on the cart page.
            if ($page == 'cart' && is_cart()) {
                return true;
            }

            // Returns true on the checkout page.
            if ($page == 'checkout' && is_checkout()) {
                return true;
            }

            // Returns true when viewing a WooCommerce endpoint
            if ($page == 'endpoint_url' && is_wc_endpoint_url()) {
                return true;
            }
        } else {
            return false;
        }
    }
}
