<?php

if (!defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly

final class Astro_Woo_Floating_Cart
{

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function __construct()
    {

        add_action('wp_enqueue_scripts', array($this, 'register_style'), 99);
        add_action('wp_enqueue_scripts', array($this, 'register_script'), 99);

        // load domain
        add_action('after_setup_theme', array($this, 'load_domain'));

        // Floating cart
        add_action('wp_footer', array($this, 'added_floating_cart'));
        add_action('woocommerce_add_to_cart_fragments', array($this, 'cart_fragment'));
        add_action('woocommerce_add_to_cart_fragments', array($this, 'cart_count_fragment'));

        // ajax cart on single page
        add_action('wp_enqueue_scripts', array($this, 'cart_load_ajax'));
        add_action('wp_ajax_astro_woo_floating_cart_single', array($this, 'cart_result'));
        add_action('wp_ajax_nopriv_astro_woo_floating_cart_single', array($this, 'cart_result'));

    }

    public function register_style()
    {

        wp_enqueue_style('font-awesome-5', astro_get_plugin_asset('css/fontawesome-all.min.css'), '', '5.3.1', 'all');
        wp_enqueue_style('retheme-base', astro_get_plugin_asset('css/retheme-base.min.css'), '', '1.0.0', 'all');
        wp_enqueue_style('astro-woo-floating-cart', astro_get_plugin_asset('css/astro.min.css'), '', '1.0.0', 'all');

    }

    public function register_script()
    {
        wp_enqueue_script('jquery');
        wp_enqueue_script('velocity', astro_get_plugin_asset('js/velocity.min.js'), false, '1.5.0', true);
        wp_enqueue_script('velocity-ui', astro_get_plugin_asset('js/velocity.ui.min.js'), false, '5.2.0', true);
        wp_enqueue_script('astro-woo-floating-cart', astro_get_plugin_asset('js/main.js'), array('jquery'), '1.0.0', true);

    }

    public function load_domain()
    {
        load_plugin_textdomain('astro_domain');
    }

    /**
     * Added floating cart on theme
     */
    public function added_floating_cart()
    {
        if(!astro_is_woocommerce('cart') && !astro_is_woocommerce('checkout') ){
             astro_get_template_part('views/floating-cart');
        }
    }

    /**
     * update cart items on mini cart
     *
     * @param [type] $fragments data items
     * @return html items
     */
    public function cart_fragment($fragments)
    {
        ob_start();

        astro_get_template_part('views/fragments-cart');

        $fragments['.awc-cart__body'] = ob_get_clean();

        return $fragments;

    }

/**
 * update cart count mini cart trigger
 *
 * @param [type] $fragments data items
 * @return count
 */
    public function cart_count_fragment($fragments)
    {
        global $woocommerce;

        ob_start();

        echo '<div class="awc-cart__count js-awc-cart-total">' . $woocommerce->cart->cart_contents_count . '</div>';

        $fragments['.js-awc-cart-total'] = ob_get_clean();

        return $fragments;

    }

    /**
     * laod ajax scripts
     */
    public function cart_load_ajax()
    {
        wp_enqueue_script('awc-floating-cart-ajax', astro_get_plugin_asset('js/add-to-cart.js'), array('jquery', 'astro-woo-floating-cart'), false, true);

        wp_localize_script(
            'astro_woo_floating_cart_ajax',
            'astro_woo_floating_object',
            array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'check_nonce' => wp_create_nonce('rml-nonce'),
            )
        );
    }


}

new Astro_Woo_Floating_Cart();
