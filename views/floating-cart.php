<div id="awc-cart" class="awc-cart js-awc-cart" data-animatein="transition.expandIn" data-duration="300">

    <div class="awc-cart__overlay js-awc-cart-close"></div>

    <div class="awc-cart__inner">

       <?php astro_get_template_part('views/fragments-cart'); ?>
        
    </div>

    <a class='awc-cart__trigger'>
        <i class="fas fa-shopping-basket awc-cart-icon-open js-awc-cart-trigger"></i>
        <i class="fas fa-times awc-cart-icon-close js-awc-cart-close"></i>
        <div class="awc-cart__count js-awc-cart-total">0</div>
    </a>

</div>
