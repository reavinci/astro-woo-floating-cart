!(function($) {
  $.fn.floating_cart = function(args) {
    return this.each(function() {
      var element = $(this);

      var defaults = {
        animatein: "transition.fadeIn",
        animateout: "transition.fadeOut",
        duration: "300",
        easing: "easeIn",
        callback: ""
      };

      var meta = element.data();
      var options = $.extend(defaults, args, meta);

      var trigger = $(".js-awc-cart-trigger");
      var panel = $(".js-awc-cart").find(".awc-cart__inner");
      var overlay = $(".js-awc-cart").find(".awc-cart__overlay");
      var remove = $(".js-awc-cart-close");

      // open
      var open = function(el) {
        panel.velocity("transition.expandIn",{ duration: options.duration }).stop();
        overlay.velocity("transition.fadeIn", { duration: 300 }).stop();
        trigger.parent().addClass('is-active');
        $("html").css("overflow-y", "hidden");
       
      };

      var close = function(el) {
        panel.velocity("transition.expandOut", { duration: options.duration }).stop();
        overlay.velocity("transition.fadeOut", { duration: 300 }).stop();
        trigger.parent().removeClass('is-active');
        $('html').css('overflow-y', 'scroll');
      };

      if (options.action === "open") {
        open();
      }

      trigger.on("click", function(event) {
        open();
        event.preventDefault();
      });


      remove.on("click", function(event) {
        close();
        event.preventDefault();
      });
    }); /*end each*/
  }; /*end plugin*/
})(window.jQuery);
